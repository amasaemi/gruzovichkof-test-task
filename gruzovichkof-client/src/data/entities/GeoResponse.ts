export default interface GeoResponse {
    name: string,
    description: string,
    coords: string
}