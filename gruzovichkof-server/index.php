<?php
    use Kreait\Firebase\Factory;
    use Kreait\Firebase\ServiceAccount;

    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . $_ENV["GSA"]);

    // создаем экземпляр Firebase
    $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri($_ENV["FFU"])
        ->create();

    создаем экземпляр Firestore
    $database = $firebase->getDatabase();

    // выполняем запрос до геосервиса, ответ приводим к объекту
    $response = json_decode(file_get_contents("https://geocode-maps.yandex.ru/1.x/?format=json&geocode=" . $_GET['query']));
    
    // создаем массив результатов поиска с полями 'name', 'description' и 'coords'
    $arrayOfResultItems = array_map(function($item) { return array('name' => $item->GeoObject->name, 'description' => $item->GeoObject->description, 'coords' => $item->GeoObject->Point->pos); }, $response->response->GeoObjectCollection->featureMember);

    пушим результат в Firestore
    $searchRequest = $database
        ->getReference('geosearch/results')
        ->push([
            'query' => $_GET['query'],
            'items' => json_encode($arrayOfResultItems)
    ]);
    
    echo json_encode($arrayOfResultItems);
?>